from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

app_name = 'api'

urlpatterns = []

# User Auth (JWT)
urlpatterns += [path('auth/', include('users.urls'))]

# Store
urlpatterns += [path('store/', include('store.urls'))]

# Docs
urlpatterns += [
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('swagger/', SpectacularSwaggerView.as_view(url_name='api:schema'), name='swagger'),
]
