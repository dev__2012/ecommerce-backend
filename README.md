# Getting Started

## Summary

- [Getting Started](#getting-started)
  - [Summary](#summary)
  - [About](#about)
  - [Dependencies](#dependencies)
    - [Installing](#installing)
  - [Running](#running)
    - [Configuring a .env file](#configuring-a-env-file)
    - [Composing the services](#composing-the-services)
      - [Docker Networks](#docker-networks)
      - [Launching the services with docker-compose](#launching-the-services-with-docker-compose)

## About

This project serves a main backend to the ecommerce app

## Dependencies

- Python 3.X
  - Poetry
- Docker

### Installing

This command will install the packages needed on PyPI:

```bash
poetry install
```

Then, enter on the shell:

```bash
poetry shell
```

## Running

### Configuring a .env file

To compose the services, we need to configure the `.env` file, so see on the project structure, we have a `.env.example` file, create a `.env` file with te same `.env.example` structure, then configure the variables.

### Composing the services

#### Docker Networks

Firstly, we need to configure the services network, otherwhise the services will not be able to communicate, so, run the command in an terminal:

```bash
docker network create ecommerce-network
```

#### Launching the services with docker-compose

In the project structure have a `docker-compose.yml` that can be used to compose the services needed to have the backend running!

So, let's compose these services with:

```bash
docker-compose up --build -d
```
