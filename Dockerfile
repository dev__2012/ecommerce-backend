FROM python
WORKDIR /app

COPY . .

RUN pip3 install poetry --quiet
RUN poetry config virtualenvs.create false
RUN python3 -m poetry install --only main --quiet

CMD [ "python3", "-m", "gunicorn", "-b", "0.0.0.0:8000", "config.wsgi" ]
