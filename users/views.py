from rest_framework import mixins, viewsets

from users import serializers


class UserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.UserSerializer
