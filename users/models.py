from django.contrib.auth import models as dj_auth_models
from django.db import models


class UserManager(dj_auth_models.UserManager):

    def create_user(self, email: str, password: str, **extra_fields) -> 'User':
        """Creates an user, given an email and password...

        Args:
            email (str): User e-mail
            password (str): User raw password

        Raises:
            ValueError: If the email or password was not given

        Returns:
            User: The populated and configured User model
        """

        if not email or not password:
            raise ValueError('Email and Password must be set!')

        user = self.model(email=self.normalize_email(email))

        user.set_password(password)

        if extra_fields.get('is_superuser'):
            user.is_superuser = True
            user.is_staff = True

        user.is_active = True

        user.save()

        return user

    def create_superuser(self, email: str, password: str, **extra_fields) -> 'User':
        """Creates an user with super permissions, given an email and password...

        Args:
            email (str): User e-mail
            password (str): User raw password

        Returns:
            User: The populated and configured User model
        """

        model = self.create_user(email, password, is_superuser=True, **extra_fields)

        return model


class User(dj_auth_models.AbstractBaseUser):
    objects = UserManager()

    email = models.EmailField(unique=True)

    groups = models.ManyToManyField(dj_auth_models.Group, related_name='groups')
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    def has_perm(self, *_):
        return True

    def has_module_perms(self, *_):
        return True
