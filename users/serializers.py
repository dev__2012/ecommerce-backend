from rest_framework.serializers import ModelSerializer

from users import models


class UserSerializer(ModelSerializer):

    def save(self, **kwargs):
        user = models.User.objects.create(email=self.validated_data['email'])
        user.set_password(self.validated_data['password'])
        user.save()
        return user

    class Meta:
        model = models.User
        fields = ['email', 'password']
        extra_kwargs = {'password': {'write_only': True}}
