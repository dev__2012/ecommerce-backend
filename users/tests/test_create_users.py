import pytest

from users import models as users_models


@pytest.mark.django_db
def test_create_user():
    user = users_models.User.objects.create_user(email="test@testing.com", password="testing123")

    assert user.is_staff == False
    assert user.is_superuser == False
    assert user.is_active == True


@pytest.mark.django_db
def test_create_superuser():
    superuser = users_models.User.objects.create_superuser(email="test@testing.com", password="testing123")

    assert superuser.is_staff == True
    assert superuser.is_superuser == True
    assert superuser.is_active == True
