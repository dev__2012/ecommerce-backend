from django.test import TestCase

from store import models as store_models
from users import models as users_models


# Create your tests here.
class OrderTestCase(TestCase):

    def setUp(self) -> None:
        users_models.User.objects.create_user(email="testing@testing.com", password="MatrixTester")

    def test_order_total(self):
        """Test case to compute the total price of an order.
        """

        user = users_models.User.objects.get(email="testing@testing.com")

        products = [
            store_models.Product.objects.create(name=f"Product {i}", price=i * 2, categories=["Fruit"])
            for i in range(1, 6)
        ]

        order = store_models.Order.objects.create(owner=user)

        order.items.add(*[
            store_models.OrderItem.objects.create(product=product, quantity=i * 2)
            for i, product in enumerate(products, start=1)
        ])

        self.assertEqual(order.total, 220)

    def test_order_item_total(self):
        """Test case to compute the total of an item of an order.
        """

        product = store_models.Product.objects.create(name="Product X", price=10, categories=["Meat"])
        quantity = 3

        order_item = store_models.OrderItem.objects.create(product=product, quantity=quantity)

        self.assertEqual(order_item.total, 30)
