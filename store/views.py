"""Contains the micro parts that composes the business rule of store.
"""

from rest_framework import permissions, viewsets

from store import models, serializers


class ProductViewSet(viewsets.ModelViewSet):
    """Control the CRUD operations for store products.
    """

    queryset = models.Product.objects.all()
    serializer_class = serializers.ProductSerializer
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]
