import pytest
import pytest_cases

from . import test_orders_cases


@pytest.mark.django_db
@pytest_cases.parametrize_with_cases('order,expected', cases=test_orders_cases.CasesOrderTotal)
def test_order_total(order, expected):
    assert order.total == expected


@pytest.mark.django_db
@pytest_cases.parametrize_with_cases('order_item,expected', cases=test_orders_cases.CasesOrderItemTotal)
def test_order_item_total(order_item, expected):
    assert order_item.total == expected
