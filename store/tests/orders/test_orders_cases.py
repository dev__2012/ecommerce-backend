from store import models as store_models


class CasesOrderTotal:

    def case_order_with_one_product(self, db, user, products):
        order = store_models.Order.objects.create(owner=user)

        order.items.add(store_models.OrderItem.objects.create(product=products[0], quantity=2))

        return order, 4

    def case_order_with_two_products(self, db, user, products):
        order = store_models.Order.objects.create(owner=user)

        order.items.add(store_models.OrderItem.objects.create(product=products[0], quantity=2))
        order.items.add(store_models.OrderItem.objects.create(product=products[1], quantity=2))

        return order, 12


class CasesOrderItemTotal:

    def case_order_item_with_one_unit(self, db, products):
        order_item = store_models.OrderItem.objects.create(product=products[0], quantity=1)

        return order_item, 2

    def case_order_item_with_many_units(self, db, products):
        order_item = store_models.OrderItem.objects.create(product=products[4], quantity=15)

        return order_item, 150
