import pytest
from django.db.models import QuerySet

from store import models as store_models
from users import models as users_models


@pytest.fixture
def products(db) -> QuerySet[store_models.Product]:
    for i in range(1, 6):
        store_models.Product.objects.create(name=f"Product {i}", price=i * 2, categories=["Fruit"])

    return store_models.Product.objects.all()


@pytest.fixture
def user(db) -> users_models.User:
    return users_models.User.objects.create_user(email="testing@testing.com", password="MatrixTester")
