from decimal import Decimal

from django.contrib.postgres.fields import ArrayField
from django.db import models

from users import models as users_models


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=250, default="No description")
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    image = models.URLField(null=True)
    categories = ArrayField(models.CharField(max_length=50), blank=True)

    class Meta:
        verbose_name = 'product'
        verbose_name_plural = 'products'


class OrderItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    quantity = models.IntegerField(default=1)

    @property
    def total(self) -> float:
        """Computes the total price of the number of units of a product.
        """

        return float(self.product.price * Decimal(self.quantity))

    class Meta:
        verbose_name = 'order_item'
        verbose_name_plural = "order_items"


class Order(models.Model):
    owner = models.ForeignKey(users_models.User, on_delete=models.DO_NOTHING, related_name='owner')
    items = models.ManyToManyField(OrderItem, related_name='items')

    paid = models.BooleanField(default=False)

    paid_on = models.DateTimeField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)

    @property
    def total(self) -> float:
        """Computes the total price of order.
        """

        return sum(map(lambda item: item.total, self.items.all()))

    class Meta:
        verbose_name = 'order'
        verbose_name_plural = 'orders'
