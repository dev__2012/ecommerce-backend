"""Routes to access store functionalities.
"""

from rest_framework.routers import DefaultRouter

from store import views

router = DefaultRouter()

router.register('products', views.ProductViewSet)

urlpatterns = router.urls
