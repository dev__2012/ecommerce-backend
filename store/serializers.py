from rest_framework.serializers import ModelSerializer

from store import models


class ProductSerializer(ModelSerializer):
    """Serializer for the Product model
    """

    class Meta:
        model = models.Product
        fields = ["id", "name", "description", "price", "image", "categories"]
        depth = 1
